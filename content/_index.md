## Discover, Learn, and Share

Download the Station app today to browse a free library of Courses for your Android device. Courses are designed to help you learn today and Station allows you to share your favorites easily with people around you.

<center>

|[Download Station for Android](/apk/app-station-release.apk)|[![Download Station](images/download_96.png)](/apk/app-station-release.apk)|
|:-----------------------------------------------------:|:--:|

</center>

---

![](images/station_home.png)

### What is Station?

Station is an F-Droid client that is tailored specifically for sharing Courses whether you have internet access or not. We hope that Station will make it easier for training organizations, individual trainers, and ad hoc groups such as activists to learn together via their mobile device!

Station, like F-Droid, utilizes "repos" or repositories to distribute apps. Station's default repo contains Courses. You can use Station to access any repo that works on an F-Droid client.

### What are Courses?

Courses are training materials and guides packaged as individual apps. Small World News' uses a tool known as Activist Apprentice to convert markdown files and images into standalone apps, known as Courses. Station comes with a [selection of Courses](https://smallworldnews.gitlab.io/station-repo/fdroid/repo/) preloaded for easy download.

You can use Station to access repos that distribute other Courses besides those that come by default. In addition to Courses, some repos may be used to distribute relevant apps, such as bundling encryption apps in a repo that distributes training materials about mobile encryption.


### Why no iPhone support?

Station is an F-droid client used for accessing F-droid repos. iOS does not support third party repos of any kind. At this time we do not expect a release of Station in its current form to be possible.

It is possible that we will release a modified version of Station that distributes some or all of the Courses in one larger app. At this time it is possible to load Course apps built with the Activist Apprentice tool on iOS devices via the iOS version of the Expo Mobile app.

### How do I get my training materials or Courses added to Station?

Small World News maintains the default repository or repo for Station. If you would like your content to be available in the default repo, please email smallworldnews at gmail dot com, with the subject line: **Content for Station Repo** and someone will get back to you as soon as possible.

If you want to use your own repo, you can load it from inside Station. This is accessed via Settings-Repositories
and then clicking the plus button. Additionally you can load repos via QR code if there is a QR scanner installed on the device.

Small World News hosts a tool called Repomaker which is what we use to build our default repo. If you're interested in accessing that tool, please contact us by emailing smallworldnews at gmail dot com, with the subject line: **Using Repomaker?**

---

### Prefer to use F-Droid?

Maybe you're already using F-droid and are not interested in having it on your phone twice, no problem. You can add the Station Courses repo to your list of available of repos.

| Add the Station Course Repo to F-droid with this QR code. |![Station Course Repo QR Code](images/qrcode_stationCourses.png)|
|-----------------------------------------------------:|:---------------:|

If you'd prefer to add it manually here are the details necessary:

#### Station Courses - Address

* `fdroidrepos://gitlab.com/smallworldnews/station-repo/raw/master/fdroid/repo`

#### Station Course Repo - Fingerprint

* `CDFEED2FAC1AA15AC623E65B3752EB9BB162B5F41D2C672AF65B5D6571796BB1`

---
