# About Station

Station is an F-Droid client app built by Torsten Grote for Small World News. Station is aimed at distributing Courses, but can be used to distribute any file type F-Droid supports.

Torsten Grote's projects can be found [here](https://gitlab.com/grote).

Station is maintained by [Small World News](https://gitlab.com/smallworldnews/).

Station is free and open source software. You can find the source code and license on [Gitlab.com](https://gitlab.com/smallworldnews/station).
