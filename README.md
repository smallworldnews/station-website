![Build Status](https://gitlab.com/smallworldnews/station-website/badges/master/build.svg)

---

This is the Repo for the publicly facing website for the Station app.

---

## URLS

Short: [https://www.getstation.org](www.getstation.org)
Complete: [https://smallworldnews.gitlab.io/station-website/](https://smallworldnews.gitlab.io/station-website/)

---

[You can review the README for this theme further to learn how to set it up for your own site as well.](themes/beautifulhugo/README.md)

----
